<?php

namespace Drupal\menu_parent_select_filter\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Renders select filter widget.
 *
 * @FormElement("select_filter")
 */
class SelectFilter extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $info = parent::getInfo();
    $class = static::class;
    $info['#process'][] = [$class, 'processFormElement'];
    return $info;
  }

  /**
   * Process render array.
   *
   * @param array $element
   *   Render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $complete_form
   *   Unused variable.
   *
   * @return array
   *   Render array.
   */
  public static function processFormElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    // Prevent endless loop.
    if (!empty($element['#already_processed'])) {
      return $element;
    }
    $orig_element = $element;

    $element = [
      '#tree' => TRUE,
      '#array_parents' => [],
    ];
    $element['menu_parent'] = [
      '#type' => 'select',
      // Prevent endless loop.
      '#already_processed' => TRUE,
      '#parents' => $orig_element['#parents'],
      '#array_parents' => [],
    ] + $orig_element;
    $element['menu_filter'] = [
      '#type' => 'textfield',
      '#title' => t('Filter parent link menu (below)'),
      '#parents' => [$orig_element['#parents'][0]],
      '#array_parents' => [],
      '#name' => 'menu[menu_filter]',
      '#id' => 'edit-menu-menu-filter',
      '#attributes' => [
        'class' => ['menu-parent-filter'],
      ],
    ];
    return $element;
  }

}
