/**
 * @file
 * Menu parent select filter behaviors.
 */

(function ($, Drupal) {
  "use strict";

  /**
   * Behavior description.
   */
  Drupal.behaviors.menuParentSelectFilter = {
    // Init to empty element; will contain the list of menu items.
    menuItems: [],
    // Element for the select menu.
    menuElement: $(".menu-parent-select")[0],
    // Element for the filter field.
    menuFilterElement: $(".menu-parent-filter")[0],

    attach: function (context, settings) {
      if (context !== document) {
        return;
      }

      // Get the initial list of options.
      for (let item = 0; item < Drupal.behaviors.menuParentSelectFilter.menuElement.length; item++) {
        Drupal.behaviors.menuParentSelectFilter.menuItems.push(Drupal.behaviors.menuParentSelectFilter.menuElement[item]);
      }

      // Run whenever there's input change for the menu parent filter field.
      $(Drupal.behaviors.menuParentSelectFilter.menuFilterElement).keyup(
        function (el) {
          // Store the currently selected option.
          const selectedItemValue = Drupal.behaviors.menuParentSelectFilter.menuElement.options[Drupal.behaviors.menuParentSelectFilter.menuElement.selectedIndex].value;

          // Empty the menu.
          Drupal.behaviors.menuParentSelectFilter.removeAll();

          // Add menu items that matches the filter.
          for (const menuitem of Drupal.behaviors.menuParentSelectFilter.menuItems) {
            // Compare against field value.
            if (menuitem.text.indexOf(el.target.value) !== -1) {
              // Create an option element.
              const option = document.createElement("option");
              option.text = menuitem.text;
              option.value = menuitem.value;
              // Append the option element.
              Drupal.behaviors.menuParentSelectFilter.menuElement.add(option, null);
            }
          }

          // Select the previously selected option.
          for (let item = 0; item < Drupal.behaviors.menuParentSelectFilter.menuElement.length; item++) {
            if (Drupal.behaviors.menuParentSelectFilter.menuElement.options[item].value === selectedItemValue) {
              Drupal.behaviors.menuParentSelectFilter.menuElement.selectedIndex = item;
            }
          }
        });
    },

    // Remove all items from a select list HTML element.
    removeAll: function () {
      for (let item = Drupal.behaviors.menuParentSelectFilter.menuElement.length; item > 0; item--) {
        Drupal.behaviors.menuParentSelectFilter.menuElement.remove(item);
      }
    },
  };
})(jQuery, Drupal);
